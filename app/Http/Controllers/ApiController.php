<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use File;
use DB;
use Carbon\Carbon;
use GuzzleHttp\Client;
use DateTime;
use App\Models\Boutique;
use App\Models\Brand;
use App\Models\Brandboutique;
use App\Models\Catboutique;
use App\Models\Discountboutique;
use App\Models\Category;
use App\Models\Discount;
use App\Models\Favorite;
use App\Models\Gallery;
use App\Models\Market;
use App\Models\Media;
use App\Models\Review;
use App\Models\Search;
use App\Models\Statistic;
use App\Models\User;
use App\Models\Checkcode;
use Artisan;
use Illuminate\Support\Facades\Response;

class ApiController extends Controller
{
    public function Register(Request $request){
        $rules = [
            'phone' => 'required',
        ];
        $validator = $this->validator($request->all(),$rules);
        if($validator->fails()) {
            $result['statusCode'] = 400;
            $result['message'] = $validator->errors();
            $result['result'] = [];
        }
        else {
            $user = User::where('phone',$request['phone'])->first();
            include 'lol.php';
            $code = random_int(1000, 9999);
            if (!$user){
                $check = new Checkcode();
                $check->phone = $request['phone'];
                $check->code = $code;
                $check->save();

                send_sms($request['phone'], 'Baraholka. Ваш пароль: '.$code);

                $result['statusCode'] = 200;
                $result['message'] = "success sms send";
                $result['result'] = $check;
            }
            else{
                $result['statusCode'] = 400;
                $result['message'] = "exist";
                $result['result'] = [];
            }

        }
        return response()->json($result, $result['statusCode']);
    }
    public function CheckCode(Request $request){
        $rules = [
            'code' => 'required',
            'phone' => 'required',
        ];
        $validator = $this->validator($request->all(),$rules);
        if($validator->fails()) {
            $result['statusCode'] = 400;
            $result['message'] = $validator->errors();
            $result['result'] = [];
        }
        else {
            $check = Checkcode::where('phone',$request['phone'])
                ->where('code',$request['code'])
                ->orderBy('id','DESC')
                ->first();

            if ($check){
                $user  = User::where('phone',$check->phone)->first();
                if (!$user){
                   $user = new User();
                   $user->phone = $check->phone;
                   $user->name =  $check->name;
                   $user->role = 'user';
                   $user->token =  str_random(30);
                   $user->save();

                   $result['statusCode'] = 200;
                   $result['message'] = "success";
                   $result['result'] = $user;
                }else{
                   $result['statusCode'] = 200;
                   $result['message'] = "success";
                   $result['result'] = $user;
                }
            }
            else{

                $result['statusCode'] = 404;
                $result['message'] = "not found";
                $result['result'] = [];

            }
        }
        return response()->json($result, $result['statusCode']);
    }
    public function WriteReview(Request $request){
        $rules = [
            'token' => 'required|exists:users,token',
            'boutique_id' => 'required|exists:boutiques,id',
            // 'review' => 'required',
            'star' => 'required|integer',
        ];
        $validator = $this->validator($request->all(),$rules);
        if($validator->fails()) {
            $result['statusCode']= 400;
            $result['message']= $validator->errors();
            $result['result']= [];
        }else {
            $user = User::where('token', $request['token'])->first();
            if ($user != null) {
                $review = new Review();
                $review->user_id = $user->id;
                $review->boutique_id = $request['boutique_id'];
                if (isset($request['review'])) {
                    $review->review = $request['review'];
                }
                $review->star = $request['star'];
                $review->save();

                $result['statusCode'] = 200;
                $result['message'] = 'Success!';
                $result['result'] = $review;
            }
            else{
                $result['statusCode'] = 404;
                $result['message'] = 'User is not exists';
                $result['result'] = null;
            }
        }
        return response()->json($result, $result['statusCode']);
    }
    public function Review($boutique_id){
        $reviews = Review::where('boutique_id', $boutique_id)->get();
        if (count($reviews)>0) {
            $result['statusCode'] = 200;
            $result['message'] = 'success';
            foreach ($reviews as $review) {
                $result['result'][] = $this->GetReview($review->id);
            }
        }else{
            $result['statusCode'] = 404;
            $result['message'] = 'Not found';
            $result['result'] = null;
        }
        return response()->json($result, $result['statusCode']);
    }
    public function UserReview($user_id){
        $reviews = Review::where('user_id', $user_id)->get();
        if (count($reviews)>0) {
            $result['statusCode'] = 200;
            $result['message'] = 'success';
            foreach ($reviews as $review) {
                $result['result'][] = $this->GetReview($review->id);
            }
        }else{
            $result['statusCode'] = 404;
            $result['message'] = 'Not found';
            $result['result'] = null;
        }
        return response()->json($result, $result['statusCode']);
    }
    public function GetReview($id){
        $review = Review::find($id);
        if ($review) {
            $item['id'] = $review['id'];
            $item['boutique'] = Boutique::find($review->boutique_id);
            $item['user'] = User::find($review->user_id);
            $item['star'] = $review['star'];
            $item['review'] = $review['review'];
            $item['updated_at'] = Carbon::parse($review['updated_at'])->format('Y-m-d h:i:s');
            $item['created_at'] = Carbon::parse($review['created_at'])->format('Y-m-d h:i:s');
            return $item;
        }else{
            return null;
        }
    }
    public function Gallery($boutique_id){
        $galleries = Gallery::where('parent_id', $boutique_id)->get();
        if (count($galleries)>0) {
            $gallery = [];
            foreach ($galleries as $key) {
                $gallery[] = $this->GetMedia($key->media_id);
            }
            $result['statusCode'] = 200;
            $result['message'] = 'Success!';
            $result['result'] = $gallery;
        }else{
            $result['statusCode'] = 404;
            $result['message'] = 'No gallery';
            $result['result'] = null;
        }
        return response()->json($result, $result['statusCode']);
    }
    public function GetDatabase(Request $request){
        $rules = [
            'password' => 'required',
        ];
        $validator = $this->validator($request->all(),$rules);
        if($validator->fails()) {
            $result['statusCode'] = 400;
            $result['message'] = $validator->errors();
            $result['result'] = [];
        }else {
            if ($request['password'] == 'baraholka!!123') {
                Artisan::call('db:dump');
                $file = "qNyIuxhYhdnjuchR2ZteOYXxIBTnAtHn0zBS4in6/baraholka.sqlite";
                
                $result['statusCode'] = 200;
                $result['message'] = 'Success!';
                $result['result'] = asset($file);
            }
            else{
                $result['statusCode'] = 404;
                $result['message'] = 'Password is incorrect';
                $result['result'] = null;
            }
        }
        return response()->json($result, $result['statusCode']);
    }
    public function Profile(Request $request){
        $rules = [
            'token' => 'required|exists:users,token',
            'avatar' => 'file|mimes:jpeg,png,jpg|max:2048',
            'name' => 'string'
        ];
        $validator = $this->validator($request->all(),$rules);
        if($validator->fails()) {
            $result['statusCode'] = 400;
            $result['message'] = $validator->errors();
            $result['result'] = [];
        }
        else {
            $user = User::where('token',$request['token'])->first();
            if ($user){
                if (isset($request['name'])) {
                    $user->name = $request['name'];
                }
                if (isset($request['avatar'])) {
                    $this->deletefile($user->avatar);
		    $user->avatar = $this->uploadfile($request['avatar']);
                }
                $user->save();

                $result['statusCode'] = 200;
                $result['message'] = 'success';
                $result['result'] = $user;
            }
            else{
                $result['statusCode'] = 404;
                $result['message'] = 'User not found';
                $result['result'] = [];
            }

        }
        return response()->json($result, $result['statusCode']);
    }
    public function Boutique(Request $request){
	    $rules = [
            'token' => 'required|exists:users,token',
            'boutique_id' => 'required|exists:boutiques,id',
            'title' => 'string',
            'avatar' => 'file|mimes:jpeg,png,jpg|max:2048',
            'contacts' => 'string',
            'links' => 'string',
            'work_hour' => 'string',
            'information' => 'string',
            'market_id' => 'integer|exists:markets,id',
            'floor' => 'integer',
            'row' => 'integer',
            'number' => 'string',
            'wholesale' => 'boolean',
            'retail' => 'boolean',
            'cash' => 'boolean',
            'credit_card' => 'boolean',
            'brands' => 'array|exists:brands,id',
            'brands.*.id' => 'distinct',
            'cats' => 'array|exists:categories,id',
            'cats.*.id' => 'distinct',
        ];
        $validator = $this->validator($request->all(),$rules);
        if($validator->fails()) {
            $result['statusCode'] = 400;
            $result['message'] = $validator->errors();
            $result['result'] = [];
        }
        else {
            $user = User::where('token',$request['token'])->first();
            $boutique = Boutique::find($request['boutique_id']);
	        // if (($user)&&($boutique)){
                if ($user->id == $boutique->owner_id) {
                    if (isset($request['title'])) {
                        $boutique->title = $request['title'];
                    }
                    if (isset($request['avatar'])) {
                        $this->deletefile($boutique->avatar);
                        $boutique->avatar = $this->uploadfile($request['avatar']);
                    }
                    if (isset($request['contacts'])) {
                        $boutique->contacts = $request['contacts'];
                    }
                    if (isset($request['links'])) {
                        $boutique->links = $request['links'];
                    }
                    if (isset($request['work_hour'])) {
                        $boutique->work_hour = $request['work_hour'];
                    }
                    if (isset($request['information'])) {
                        $boutique->information = $request['information'];
                    }
                    if (isset($request['market_id'])) {
                        $boutique->market_id = $request['market_id'];
                    }
                    if (isset($request['floor'])) {
                        $boutique->floor = $request['floor'];
                    }
                    if (isset($request['row'])) {
                        $boutique->row = $request['row'];
                    }
                    if (isset($request['number'])) {
                        $boutique->number = $request['number'];
                    }
                    if (isset($request['wholesale'])) {
                        $boutique->wholesale = $request['wholesale'];
                    }
                    if (isset($request['retail'])) {
                        $boutique->retail = $request['retail'];
                    }
                    if (isset($request['cash'])) {
                        $boutique->cash = $request['cash'];
                    }
                    if (isset($request['credit_card'])) {
                        $boutique->credit_card = $request['credit_card'];
                    }
                    if(isset($request['brands'])){
                        $brandboutique = Brandboutique::where('boutique_id', $boutique->id)->get();
                        foreach ($brandboutique as $key) {
                            $key->delete();
                        }
                        foreach ($request['brands'] as $brand) {
                            $brand_b = new Brandboutique();
                            $brand_b->boutique_id = $user->id;
                            $brand_b->brand_id = $brand;
                            $brand_b->save();
                        }
                    }
                    if(isset($request['cats'])){
                        $catboutique = Catboutique::where('boutique_id', $boutique->id)->get();
                        foreach ($catboutique as $key) {
                            $key->delete();
                        }
                        foreach ($request['cats'] as $cat) {
                            $cat_b = new Catboutique();
                            $cat_b->boutique_id = $user->id;
                            $cat_b->cat_id = $cat;
                            $cat_b->save();
                        }
                    }
                    if(isset($request['discounts'])){
                        $discounts = Discount::where('boutique_id', $boutique->id)->get();
                        foreach ($discounts as $key) {
                            $key->delete();
                        }
                        foreach ($request['discounts'] as $discount) {
                            $discount_b = new Discount();
                            $discount_b->boutique_id = $user->id;
                            $discount_b->percentage = $discount;
                            $discount_b->save();
                        }
                    }
                    $boutique->save();

                    $result['statusCode'] = 200;
                    $result['message'] = 'success';
                    $result['result'] = $this->GetBoutique($boutique->id);
                }else{
                    $result['statusCode'] = 404;
                    $result['message'] = 'Access denied';
                    $result['result'] = null;
                }
            // }
            // else{
            //     $result['statusCode'] = 404;
            //     $result['message'] = 'User not found';
            //     $result['result'] = [];
            // }
        }
        return response()->json($result, $result['statusCode']);
    }
    public function DeleteImages(Request $request){
        $rules = [
            'token' => 'required|exists:users,token',
            'boutique_id' => 'required|exists:boutiques,id',
            'images' => 'array|exists:media,id',
            'images.*.id' => 'distinct',
        ];
         $validator = $this->validator($request->all(),$rules);
        if($validator->fails()) {
            $result['statusCode'] = 400;
            $result['message'] = $validator->errors();
            $result['result'] = [];
        }
        else {
            $user = User::where('token',$request['token'])->first();
            $boutique = Boutique::find($request['boutique_id']);
            if ($user->id == $boutique->owner_id) {
                if (isset($request['images'])) {
                    foreach ($request['images'] as $image) {
                        $media[] = Gallery::where('parent_id', $boutique->id)->where('media_id', $image)->first();
                    }
                    foreach ($media as $key) {
                        $item = Media::find($key->media_id);
                        $this->deletefile($item->path);
                        $item->delete();
                    }
                    foreach ($media as $key){
                        $key->delete();
                    }
                }
                $result['statusCode'] = 200;
                $result['message'] = 'success';
                $result['result'] = $this->Gallery($boutique->id)->original;
            }else{
                $result['statusCode'] = 404;
                $result['message'] = 'Access denied';
                $result['result'] = null;
            }
        }
        return response()->json($result, $result['statusCode']);
    }
    public function AddImages(Request $request){
        $rules = [
            'token' => 'required|exists:users,token',
            'boutique_id' => 'required|exists:boutiques,id',
            'images' => 'array',
            'images.*.id' => 'distinct|file|mimes:jpeg,png,jpg|max:2048',
        ];
         $validator = $this->validator($request->all(),$rules);
        if($validator->fails()) {
            $result['statusCode'] = 400;
            $result['message'] = $validator->errors();
            $result['result'] = [];
        }
        else {
            $user = User::where('token',$request['token'])->first();
            $boutique = Boutique::find($request['boutique_id']);
            if ($user->id == $boutique->owner_id) {
                if (isset($request['images'])) {
                    foreach ($request['images'] as $image) {
                        $media = new Media();
                        $media->path = $this->uploadfile($image);
                        $media->save();
                        $gallery = new Gallery();
                        $gallery->parent_id = $boutique->id;
                        $gallery->media_id = $media->id;
                        $gallery->save();

                    }
                }
                $result['statusCode'] = 200;
                $result['message'] = 'success';
                $result['result'] = $this->Gallery($boutique->id)->original;
            }else{
                $result['statusCode'] = 404;
                $result['message'] = 'Access denied';
                $result['result'] = null;
            }
        }
        return response()->json($result, $result['statusCode']);
    }
    public function GetBoutique($boutique_id){
        $boutique = Boutique::find($boutique_id);
        if ($boutique) {
            $result['statusCode'] = 200;
            $result['message'] = 'success';
            $result['result'] = $boutique;
            $result['result']['brands'] = $this->GetBrandsBoutique($boutique->id);
            $result['result']['cats'] = $this->GetCatsBoutique($boutique->id);
            $result['result']['discounts'] = $this->GetDiscountsBoutique($boutique->id);
        }else{
            $result['statusCode'] = 404;
            $result['message'] = 'not found';
            $result['result'] = null;
        }
        return $result['result'];
    }
    public function GetBrandsBoutique($boutique_id){
        $brands = Brandboutique::where('boutique_id', $boutique_id)->get();
        if (count($brands)>0) {
            foreach ($brands as $key) {
                $item[] = Brand::find($key->brand_id)->name;
            }
            return $item;
        }else{
            return null;
        }
    }
    public function GetCatsBoutique($boutique_id){
        $cats = Catboutique::where('boutique_id', $boutique_id)->get();
        if (count($cats)>0) {
            foreach ($cats as $key) {
                $item[] = Category::find($key->cat_id)->title;
            }
            return $item;
        }else{
            return null;
        }
    }
    public function GetDiscountsBoutique($boutique_id){
        $discounts = Discount::where('boutique_id', $boutique_id)->get();
        if (count($discounts)>0) {
            foreach ($discounts as $key) {
                $item[] = $key->percentage;
            }
            return $item;
        }else{
            return null;
        }
    }
    public function GetMedia($id){
        $media = Media::find($id);
        if ($media!=null) {
            $item['id'] = $media->id;
            $item['path'] = asset($media->path);
            return $item;
        }else{
            return null;
        }
    }
    public function uploadfile($file,$dir = 'uploads'){
        $file_type = File::extension($file->getClientOriginalName());
        $file_name = time().str_random(5).'.'.$file_type;
        $file->move($dir, $file_name);
        return $dir.'/'.$file_name;
    }
    public function deletefile($path){
        if (File::exists($path)) {
            File::delete($path);
            return true;
        }else{
            return false;
        }
    }   
    protected function validator($errors,$rules) {

        return Validator::make($errors,$rules);
    }
}

