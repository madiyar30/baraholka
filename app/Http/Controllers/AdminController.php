<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use File;
use DB;
use Carbon\Carbon;
use GuzzleHttp\Client;
use DateTime;
use App\Models\Boutique;
use App\Models\Brand;
use App\Models\Brandboutique;
use App\Models\Catboutique;
use App\Models\Category;
use App\Models\Discount;
use App\Models\Favorite;
use App\Models\Gallery;
use App\Models\Market;
use App\Models\Review;
use App\Models\Search;
use App\Models\Statistic;
use App\Models\User;

class AdminController extends Controller
{
    //
}
