<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File;

class MySqlDump extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:dump';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs the mysqldump utility using info from .env';
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ds = DIRECTORY_SEPARATOR;
        $host = env('DB_HOST');
        $username = env('DB_USERNAME');
        $password = env('DB_PASSWORD');
        $database = env('DB_DATABASE');
        
        $ts = time();
        $path = storage_path().$ds.'qNyIuxhYhdnjuchR2ZteOYXxIBTnAtHn0zBS4in6'.$ds;
        $public_path = public_path().$ds.'qNyIuxhYhdnjuchR2ZteOYXxIBTnAtHn0zBS4in6'.$ds;
        //database_path() . $ds . 'backups' . $ds . date('Y', $ts) . $ds . date('m', $ts) . $ds . date('d', $ts) . $ds;
        //$file = date('Y-m-d-His', $ts) . '-dump-' . $database . '.sql';
        File::delete($path.'baraholka.sqlite');
        $file = 'dump-' . $database . '.sql';
        $command1 = sprintf('mysqldump -h %s -u %s -p\'%s\' %s --skip-comments > %s', $host, $username, $password, $database, $path . $file);
        $command2 = sprintf($path.'./mysql2sqlite.sh '.$path.'dump-baraholka.sql | sqlite3 '.$public_path.'baraholka.sqlite');
        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }
        exec($command1);
        exec($command2);
    }
}