<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('register', 'ApiController@Register');
Route::post('check_code', 'ApiController@CheckCode');
Route::post('write_review', 'ApiController@WriteReview');
Route::get('/gallery/{id}', 'ApiController@Gallery');
Route::post('delete_images', 'ApiController@DeleteImages');
Route::post('add_images', 'ApiController@AddImages');
Route::get('/review/{id}', 'ApiController@Review');
Route::get('/user_review/{id}', 'ApiController@UserReview');
Route::get('/get_boutique/{id}', 'ApiController@GetBoutique');
Route::post('get_db', 'ApiController@GetDatabase');
Route::post('profile', 'ApiController@Profile');
Route::post('boutique', 'ApiController@Boutique');

